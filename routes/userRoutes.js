const express = require('express');
const router = express.Router();

const userController = require('../controllers/userControllers');
const auth = require('../auth');

// registration with validation
router.post(
  '/register',
  userController.checkEmailExists,
  userController.registerUser
);

// login vwith token
router.post('/login', userController.loginUser);

// update user role (only for admin)
router.patch(
  '/updateRole/:userIdToUpdate',
  auth.verify,
  userController.updateRole
);

router.get('/profile', userController.profileDetails);

module.exports = router;
