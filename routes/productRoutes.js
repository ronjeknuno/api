const express = require('express');
const router = express.Router();

const productController = require('../controllers/productControllers');
const auth = require('../auth');

// create a product
router.post('/addProduct', auth.verify, productController.addProduct);

// create bin
router.post('/addBin', auth.verify, productController.addBin);

// retrieve not empty products
router.get('/', productController.getNotEmptyProducts);

// retrieve all products
router.get('/allProducts', auth.verify, productController.getAllProducts);

// move product
router.delete(
  '/allProducts/:productID',
  auth.verify,
  productController.removeProduct
);

// check stock
router.patch('/checkStocks', productController.checkStock);

// update product
router.post(
  '/updateProduct/:productID',
  auth.verify,
  productController.updateProduct
);

// archieve/ unarchieve product
router.patch(
  '/updateProduct/:productID',
  auth.verify,
  productController.updateIsActive
);

// get single product
router.get('/:productID', productController.getProduct);

module.exports = router;
