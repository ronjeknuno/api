const Product = require('../models/productModel');
const Order = require('../models/itemModel');
const User = require('../models/userModel');
const Bin = require('../models/binModel');
const auth = require('../auth');

// create product
module.exports.addProduct = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const { name, description, price, stocks } = request.body;

  let newProduct = new Product({
    name,
    description,
    price,
    stocks,
  });

  if (userData.isAdmin) {
    console.log(newProduct);
    newProduct
      .save()
      .then((result) => {
        console.log(result);
        response.json({
          message: `${name} is added successfully!`,
          result: result,
        });
      })
      .catch((error) => {
        console.log(error);
        response.json({ status: true });
      });
  } else {
    return response.json({ message: 'You are not allowed to add Product!' });
  }
};

// set isnotempty false when stock reach zero
module.exports.checkStock = async (request, response) => {
  const result = await Product.find({
    $and: [{ stocks: { $lte: 0 } }, { isEmpty: false }],
  })
    .then((result) => {
      return result.forEach(async (element) => {
        const updatedIsEmpty = {
          isEmpty: !element.isEmpty,
        };
        try {
          const document = await Product.findByIdAndUpdate(
            element._id,
            updatedIsEmpty,
            {
              new: true,
            }
          );
          return response.send(`${document}`);
        } catch (error) {
          return response.send(error);
        }
      });
    })
    .catch((error) => response.send(error));

  console.log(result);

  //   const result = await Product.aggregate([
  //     {
  //       $match: {
  //         $and: [{ stocks: { $lte: 0 } }, { isEmpty: false }],
  //       },
  //     },
  //     {
  //       $project: {
  //         isEmpty: {
  //           $cond: { if: { $lte: ['$stocks', 0] }, then: true, else: false },
  //         },
  //       },
  //     },
  //   ]).then((result) => {
  //     response.send(result);
  //     return result;
  //     //   .save()
  //     //   .then((succcess) => true)
  //     //   .catch((error) => false);

  //     // Product.findByIdAndUpdate(result._id, result.isEmpty, { new: true })
  //     //   .then((success) => {
  //     //     response.send(true);
  //     //   })
  //     //   .catch((error) => {
  //     //     console.log(error);
  //     //     response.send(false);
  //     //   });
  //   });

  //   result.forEach(element => {
  //     console.log(elem)
  //   });
  //   Product.findByIdAndUpdate(result)
};

// retrieve not empty product
module.exports.getNotEmptyProducts = async (request, response) => {
  try {
    const result = await Product.find(
      { $and: [{ isEmpty: false }, { isActive: true }] },
      {
        isEmpty: 0,
        createdAt: 0,
        updatedAt: 0,
        __v: 0,
      }
    );
    return response.send(result);
  } catch (error) {
    console.log(error);
    response.send(error);
  }
};

// retrieve all products
module.exports.getAllProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  if (userData.isAdmin) {
    return Product.find({})
      .then((result) => response.json(result))
      .catch((error) => {
        console.log(error);
        response.json({ error: error });
      });
  } else {
    response.json({ message: `Sorry, you are not allowed in this page!}` });
  }
};

// retrieve single product
module.exports.getProduct = async (request, response) => {
  const { productID } = request.params;

  try {
    const result = await Product.findById(productID, {
      _id: 0,
      isEmpty: 0,
      createdAt: 0,
      updatedAt: 0,
      __v: 0,
    });
    response.json({ message: result });
  } catch (error) {
    response.json({ message: error });
  }
};

// update a Product
module.exports.updateProduct = (request, response) => {
  const { name, description, price, stocks, isEmpty } = request.body;
  const { productID } = request.params;
  console.log(productID);
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);

  let updatedProduct = {
    name,
    description,
    price,
    stocks,
    isEmpty,
  };

  if (userData.isAdmin) {
    Product.findByIdAndUpdate(productID, updatedProduct, { new: true }).then(
      (result) => {
        response.json({ message: `${result.name} is Updated`, result: result });
      }
    );
  } else {
    response.json({ message: `You are not allowed to update Courses!` });
  }
};

// archive / unarchive product
module.exports.updateIsActive = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const { productID } = request.params;

  if (userData.isAdmin) {
    return Product.findById(productID)
      .then((result) => {
        result.isActive = !result.isActive;
        return Product.findByIdAndUpdate(productID, result, {
          new: true,
        })
          .then((document) => {
            response.send(document);
          })
          .catch((error) => response.send(error));
      })
      .catch((error) => response.send(error));
  } else {
    response.send(`You don't have access on this page!`);
  }
};

// move product to bin
module.exports.removeProduct = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const { productID } = request.params;
  const result = await Product.findById(productID);
  const bin = await Bin.find();
  console.log(result);

  if (result) {
    let newBin = new Bin({
      productID: result._id.toString(),
      name: result.name,
      description: result.description,
      price: result.price,
      stocks: result.stocks,
      deletedOn: result.updatedAt,
    });
    if (userData.isAdmin) {
      console.log(newBin);
      newBin
        .save()
        .then((result) => {
          console.log(result._id.toString());
          // response.json(result);
          // remove
          // const remove = Product.find({ name: 'asd' });
          // console.log(remove);
        })
        .catch((error) => {
          console.log(error);
          response.json({ status: 'Error' });
        });

      const remove = await Product.findByIdAndRemove(productID, result)
        .then((result) => {
          response.json({
            message: `${result.name} is Removed`,
            result: result,
          });
        })
        .catch((error) => {
          console.log(error);
          response.json({ status: error });
        });
    } else {
      response.json({ message: `You don't have access on this page!` });
    }
  } else {
    response.json({ message: 'Product Not Exist' });
  }
};

// bin test
module.exports.addBin = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const { name, description, price, stocks } = request.body;

  let newProduct = new Bin({
    name,
    description,
    price,
    stocks,
  });

  if (userData.isAdmin) {
    newProduct
      .save()
      .then((result) => {
        console.log(result);
        response.json({
          result: result,
        });
      })
      .catch((error) => {
        console.log(error);
        response.json({ status: true });
      });
  } else {
    return response.json({ message: 'You are not allowed to add Product!' });
  }
};
