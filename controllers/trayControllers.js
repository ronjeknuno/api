const Product = require('../models/productModel');
const Item = require('../models/itemModel');
const User = require('../models/userModel');
const auth = require('../auth');
const Tray = require('../models/trayModel');
const mongoose = require('mongoose');

// Tray checker
// module.exports.checkTrayExist = async (request, response) => {
//   const userData = auth.decode(request.headers.authorization);
//   const orderData = await Order.find({ userId: userData.id });

//   if (orderData.length > 0) {
//     message = `The ${request.body.email} is already taken, please use other email.`;
//     return response.send(message);
//   } else {
//     next();
//   }
// };

// Add product to tray
module.exports.addToTray = async (request, response) => {
  const { productID } = request.params;
  const { quantity } = request.body;
  const userData = auth.decode(request.headers.authorization);
  const trayDB = await Tray.findById(userData.trayId);
  const trayItems = trayDB.items;
  const productDB = await Product.findByIdAndUpdate(productID);
  const productStock = productDB.stocks;
  console.log(userData);
  // response.send(userData);
  if (userData.isAdmin)
    return response.json({
      message: 'Admins are not allowed to add product to cart!',
    });

  if (productDB.stocks <= 0) {
    response.json({ message: `Sold Out!` });
  } else {
    // console.log(trayItems);
    // trayItems.forEach(async (item) => {
    //   const itemProducts = await Item.findById(item);
    //   console.log(itemProducts.productId);
    //   console.log(mongoose.Types.ObjectId(productID));
    //   if ("itemProducts.productId" === mongoose.Types.ObjectId(productID)) {
    //     console.log('with dup');
    //   } else {
    //     console.log('no dup');
    //   }
    // });

    // if (trayItems.includes(productID)) {
    // const itemDB = await Item.updateOne(
    //   {
    //     $and: [{ userId: userData.id }, { productId: productID }],
    //   },
    //   { $set: { quantity: quantity } }
    // );
    // console.log(itemDB);
    // }
    // if (trayItems.includes(productID)) {
    //   const itemDB = await Item.find({
    //     $and: [{ userId: userData.id }, { productId: productID }],
    //   }).then(async (result) => {
    //     result.quantity += quantity;
    //     try {
    //       const success = await result.save();
    //       return response.send(`${productDB.name} added to your tray!`);
    //     } catch (error) {
    //       return response.send(error);
    //     }
    //   });
    // }
    // else {
    const newItem = new Item({
      _id: new mongoose.Types.ObjectId(),
      productId: productID,
      quantity,
      subTotal: productDB.price * quantity,
      userId: userData.id,
    });
    newItem.save(async (error, item) => {
      if (error) {
        console.log(error);
      } else {
        const toTheTray = Tray.findById(userData.trayId).then(
          async (result) => {
            result.items.push(newItem._id);
            result.totalAmount += newItem.subTotal;

            try {
              const success = await result.save();
              return response.json(true);
            } catch (error) {
              return response.json({ message: false });
            }
          }
        );
      }
    });
  }
  // }

  // const productAddtoTray = await Item.findById(userData.orderId._id)
  //   .then(async (result) => {
  //     result.products.push({
  //       productId: productID,
  //       quantity,
  //     });
  //     result.subTotal += productDB.price * quantity;

  //     try {
  //       const success = await result.save();
  //       return response.send(`${productDB.name} added to your tray!`);
  //     } catch (error) {
  //       return response.send(false);
  //     }
  //   })
  //   .catch((error) => {
  //     console.log(error);
  //     return response.send(false);
  //   })
  //   .catch((error) => console.log(error));

  // const updateProduct = await Product.findById(productID)
  //   .then((result) => {
  //     result.stocks -= quantity;
  //     try {
  //       const success = result.save();
  //       return response.send(true);
  //     } catch (error) {
  //       return response.send(false);
  //     }
  //   })
  //   .catch((error) => {
  //     console.log(error);
  //     return response.send(false);
  //   })
  //   .catch((error) => console.log(error));

  //   if (orderData.length > 0) {
  //     Order.find({ userId: userData.id })
  //       .then((result) => {
  //         result.products.push({
  //           productId: productID,
  //           quantity,
  //         });
  //         result.subTotal += quantity * productDB.price;

  //         return result
  //           .save()
  //           .then((success) => true)
  //           .catch((error) => response.send(false));
  //       })
  //       .catch((error) => {
  //         console.log(error);
  //         return response.send(false);
  //       })
  //       .catch((error) => console.log(error));
  //   } else {
  //     Order({
  //       userId: userData.id,
  //       products: [
  //         {
  //           productId: productID,
  //           quantity,
  //         },
  //       ],
  //       subTotal: quantity * productDB.price,
  //     })
  //       .save()
  //       .then((result) => {
  //         console.log(result);
  //         response.send(true);
  //       })
  //       .catch((error) => {
  //         console.log(error);
  //         response.send(false);
  //       });
  //   }

  // if (userData.isAdmin) {
  //   response.send(`Admins are not allowed to add!`);
  // } else {
  //   response.send(`Not admin`);
  // }

  // const product = await Product.findById(productID).then((result) => {
  //   return result;
  // });
  // console.log(product.price);
  // if (userData.isAdmin) {
  //   response.send(`You are not allowed to Add!`);
  // } else {
  //   let addOrder = new Order({
  //     products: [
  //       {
  //         productId: productID,
  //       },
  //     ],
  //     subTotal: product.price,
  //   });
  //   addOrder
  //     .save()
  //     .then((result) => {
  //       console.log(result);
  //       response.send(true);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //       response.send(true);
  //     });
  // const product = Product.findById(productID);
  // let data = {
  //   productID,
  //   userID: userData.id,
  // };
  // const isProductUpdated = Product.findById(productID)
  //   .then(async (result) => {
  //     result.stocks -= 1;
  //     try {
  //       const success = await result.save();
  //       return true;
  //     } catch (error) {
  //       return response.send(false);
  //     }
  //   })
  //   .catch((error) => {
  //     console.log(error);
  //     return response.send(false);
  //   })
  //   .catch((error) => console.log(error));
  // const product = Product.findById(productID);
  // let data = {
  //   productID,
  //   userID: userData.id,
  // };
  // const isProductUpdated = Product.findById(productID)
  //   .then(async (result) => {
  //     result.stocks -= 1;
  //     try {
  //       const success = await result.save();
  //       return true;
  //     } catch (error) {
  //       return response.send(false);
  //     }
  //   })
  //   .catch((error) => {
  //     console.log(error);
  //     return response.send(false);
  //   })
  //   .catch((error) => console.log(error));
  // const isOrderUpdated = User.findById(userData.id)
  //   .then(async (result) => {
  //     console.log(result);
  //     result.orders.push({
  //       orderId: data.productID,
  //     });
  //     try {
  //       const success = await result.save();
  //       return true;
  //     } catch (error) {
  //       return response.send(false);
  //     }
  //   })
  //   .catch((error) => {
  //     console.log(error);
  //     return response.send(false);
  //   })
  //   .catch((error) => console.log(error));
  // isProductUpdated && isOrderUpdated
  //   ? response.send(`Added to tray`)
  //   : response.send(
  //       `We encounter an error in your enrollment, please try again!`
  //     );
  // }
};

// checkout items
module.exports.checkOutTray = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const trayDB = await Tray.findById(userData.trayId);
  const itemsInTray = trayDB.items;
  const { productID } = request.params;
  const productDB = await Product.findByIdAndUpdate(productID);
  // response.send(itemsInTray);

  // const itemDB = await Item.findById(itemsInTray[3]);
  // response.send(itemDB);

  if (userData.isAdmin)
    return response.json({ message: 'Admins are not allowed checkout!' });
  if (itemsInTray.length <= 0)
    return response.json({ message: 'Your Tray is empty!' });

  // const groupedItems = {};
  // for (let index = 0; index < itemsInTray.length; index++) {
  //   groupedItems[itemsInTray[index]] = groupedItems[itemsInTray[index]] ?? [];
  //   groupedItems[itemsInTray[index]].push(+itemsInTray[index]);
  // }
  // const convertToInt = [];
  // for (let index = 0; index < itemsInTray.length; index++) {
  //   convertToInt[index] = itemsInTray[]

  // }
  const CheckedOutItems = [];
  for (let index = 0; index < itemsInTray.length; index++) {
    const items = await Item.findById(itemsInTray[index], {
      quantity: 1,
      status: 1,
      subTotal: 1,
    }).populate('productId', {
      name: 1,
      description: 1,
      price: 1,
    });

    items.status = 'CheckedOut';
    console.log(items);
    CheckedOutItems.push(items);
    await items.save();

    const product = await Product.findById(items.productId._id);
    product.stocks -= items.quantity;
    await product.save();
  }
  const resetTray = Tray.findById(userData.trayId).then((result) => {
    result.items = [];
    result.totalAmount = 0;

    return result
      .save()
      .then((success) => console.log(result))
      .catch((error) => console(false));
  });
  CheckedOutItems.push(`totalAmount : ${trayDB.totalAmount}`);
  response.json({ message: CheckedOutItems });
};

// response.send(mongoose.Types.ObjectId(itemsInTray[1]));

// Show items in Tray
module.exports.Tray = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const trayDB = await Tray.findById(userData.trayId);
  const itemsInTray = trayDB.items;

  console.log(itemsInTray.length);
  if (userData.isAdmin) return response.send(`Admins don't have Tray!`);
  if (itemsInTray.length <= 0) return response.send('Your Tray is empty!');

  const itemsToTray = [];
  for (let index = 0; index < itemsInTray.length; index++) {
    const items = await Item.findById(itemsInTray[index], {
      _id: 0,
      quantity: 1,
      status: 1,
      subTotal: 1,
    }).populate('productId', {
      _id: 0,
      name: 1,
      description: 1,
      price: 1,
    });
    itemsToTray.push((items[index] = items));
  }
  itemsToTray.push(`totalAmount : ${trayDB.totalAmount}`);
  response.json({ result: itemsToTray });
};

// const userData = auth.decode(request.headers.authorization);
// const orderDB = await Tray.findById(userData.trayId._id).populate('items');
// const itemDB = await Item.findById(userData.id);

// response.send(orderDB.items);

// .then((result) => {
// if (userData.isAdmin) {
//   response.send(`You do not have access here!`);
// } else {
//   response.send(orderDB);
// }
// });

//   response.send(orderDB.products).populate('productId');

// remove item from cart

// move checkouted items to order collection

// forgot password

//
