const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const trayRoutes = require('./routes/trayRoutes');

// express app
const app = express();

// db connection
mongoose
  .connect(
    'mongodb+srv://admin:admin@zuittbatch243.1muzoob.mongodb.net/E-Commerce-DB?retryWrites=true&w=majority'
  )
  .then(() => {
    app.listen(process.env.PORT, () => {
      console.log(`Server is running in localhost: ${process.env.PORT}`);
    });
  })
  .catch((error) => {
    console.log(error);
  });

mongoose.connection.on(
  'error',
  console.error.bind(console, 'connection error')
);
mongoose.connection.once('open', () => console.log('Connected to Database.'));

// middleware
app.use(express.json());
app.use(cors());
app.use((request, response, next) => {
  console.log(request.path, request.method);
  next();
});

// routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/trays', trayRoutes);
