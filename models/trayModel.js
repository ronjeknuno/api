const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const traySchema = new Schema(
  {
    items: [
      {
        type: mongoose.Types.ObjectId,
        ref: 'Item',
      },
    ],
    totalAmount: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('Tray', traySchema);
